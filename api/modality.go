package api

import (
	"net/http"
)

type Modality struct {
	Id             string         `json:"_id",omitempty"`
	Classification Classification `json:"classification,omitempty"`
}

func (c *Client) GetAllModalities() ([]*Modality, *http.Response, error) {
	var aerr *Error
	var modalities []*Modality

	resp, err := c.New().Get("modalities").Receive(&modalities, &aerr)

	return modalities, resp, Coalesce(err, aerr)
}

func (c *Client) AddModality(modality *Modality) (*http.Response, error) {
	var aerr *Error
	var response *IdResponse

	resp, err := c.New().Post("modalities").BodyJSON(modality).Receive(&response, &aerr)

	return resp, Coalesce(err, aerr)
}

func (c *Client) GetModality(id string) (*Modality, *http.Response, error) {
	var aerr *Error
	var modality *Modality

	resp, err := c.New().Get("modalities/"+id).Receive(&modality, &aerr)

	return modality, resp, Coalesce(err, aerr)
}

func (c *Client) ReplaceModality(modality *Modality) (*http.Response, error) {
	var aerr *Error
	var response interface{}

	resp, err := c.New().Put("modalities/"+modality.Id).BodyJSON(modality).Receive(response, &aerr)

	return resp, Coalesce(err, aerr)
}

func (c *Client) DeleteModality(id string) (*http.Response, error) {
	var aerr *Error
	var response interface{}

	resp, err := c.New().Delete("modalities/"+id).Receive(response, &aerr)

	return resp, Coalesce(err, aerr)
}
