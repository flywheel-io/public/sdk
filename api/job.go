package api

import (
	"net/http"
	"time"
)

// Formula describes a single unit of work.
type Formula struct {
	Inputs  []*Input  `json:"inputs"`
	Target  Target    `json:"target"`
	Outputs []*Output `json:"outputs"`
}

// Input describes an asset that must be present for the formula to execute.
type Input struct {
	Type     string `json:"type,omitempty"`
	URI      string `json:"uri,omitempty"`
	Location string `json:"location,omitempty"`
	VuID     string `json:"vu,omitempty"`
}

// Target describes what the formula will execute.
type Target struct {
	Command []string          `json:"command,omitempty"`
	Env     map[string]string `json:"env,omitempty"`
	Dir     string            `json:"dir,omitempty"`
	Uid     uint32            `json:"uid,omitempty"`
	Gid     uint32            `json:"gid,omitempty"`
}

// Output describes the creation of an asset after a formula is complete.
type Output struct {
	Type     string `json:"type,omitempty"`
	URI      string `json:"uri,omitempty"`
	Location string `json:"location,omitempty"`
	VuID     string `json:"vu,omitempty"`
}

// Result describes the result of a formula.
type Result struct {
	ExitCode int `json:"exitcode"`
}

// FormulaResult combines a (possibly-modified) Formula with any results.
type FormulaResult struct {
	Formula

	Result Result `json:"result"`
}

// Enum for job states.
type JobState string

const (
	Pending   JobState = "pending"
	Running   JobState = "running"
	Failed    JobState = "failed"
	Complete  JobState = "complete"
	Cancelled JobState = "cancelled"
)

// A count of jobs in each state.
type JobStateCount struct {
	Pending   int `json:"pending"`
	Running   int `json:"running"`
	Failed    int `json:"failed"`
	Complete  int `json:"complete"`
	Cancelled int `json:"cancelled"`
}

// Enum for job retrieval attempts.
type JobRetrieval int

const (
	JobsAquired JobRetrieval = iota
	NoPendingJobs
	JobFailure
)

type ObsoleteJobProfile struct {
	Elapsed int64 `json:"elapsed, omitempty"`
}

type Job struct {
	Id     string `json:"id,omitempty" bson:"_id"`
	GearId string `json:"gear_id,omitempty" bson:"gear_id"`

	State         JobState `json:"state,omitempty"`
	Attempt       int      `json:"attempt,omitempty"`
	Origin        *Origin  `json:"origin,omitempty"`
	FailureReason string   `json:"failure_reason"`

	Config      map[string]interface{} `json:"config,omitempty"`
	Inputs      map[string]interface{} `json:"inputs,omitempty"`
	Destination *ContainerReference    `json:"destination,omitempty"`
	Tags        []string               `json:"tags,omitempty"`

	Request *Formula `json:"request,omitempty"`

	ResultMetadata map[string]interface{} `json:"produced_metadata,omitempty"`
	ResultFiles    []string               `json:"saved_files,omitempty"`

	Created  *time.Time `json:"created,omitempty"`
	Modified *time.Time `json:"modified,omitempty"`

	// Obsoleted by https://github.com/flywheel-io/core/pull/1446
	OldProfile *ObsoleteJobProfile `json:"profile,omitempty"`

	Profile *JobProfile `json:"profile,omitempty" bson:"profile"`
}

type JobsMatch struct {
	Group           []string `json:"group,omitempty"`
	GearName        []string `json:"gear-name,omitempty"`
	Tag             []string `json:"tag,omitempty"`
	ComputeProvider []string `json:"compute-provider,omitempty"`
}

type JobsAskType struct {
	Jobs    int  `json:"jobs,omitempty"`
	Peek    bool `json:"peek,omitempty"`
	States  bool `json:"states,omitempty"`
	Encoded bool `json:"encoded,omitempty"`
}

type JobsQuestion struct {
	Whitelist    *JobsMatch   `json:"whitelist"`
	Blacklist    *JobsMatch   `json:"blacklist"`
	Capabilities []string     `json:"capabilities"`
	Return       *JobsAskType `json:"return,omitempty"`
}

type JobsAnswer struct {
	Jobs   []*Job         `json:"jobs"`
	States *JobStateCount `json:"states"`
}

type JobCompletionTicket struct {
	// Success and Elapsed are expected by the legacy version of the endpoint
	// Obsoleted by https://github.com/flywheel-io/core/pull/1503
	Success bool  `json:"success,omitempty"`
	Elapsed int64 `json:"elapsed,omitempty"`
}

// JobTicket is returned when creating a JobCompletionTicket
type JobCompletionTicketResponse struct {
	// The ticket reference string
	Ticket string `json:"ticket,omitempty"`
}

type JobCompletion struct {
	// Whether or not the job ran successfully
	Success bool `json:"success"`

	// If Success is false, the suspected reason for the failure
	FailureReason string `json:"failure_reason,omitempty"`

	// Job profile information
	Profile *JobProfile `json:"profile,omitempty"`
}

type JobProfile struct {
	// The number of input files
	TotalInputFiles int64 `json:"total_input_files,omitempty"`

	// The combined size of all input files (bytes)
	TotalInputSizeBytes int64 `json:"total_input_size_bytes,omitempty"`

	// The number of output files
	TotalOutput_files int64 `json:"total_output_files,omitempty"`

	// The combined size of all output files (bytes)
	TotalOutput_sizeBytes int64 `json:"total_output_size_bytes,omitempty"`

	// The length of time taken to download gear container and inputs (ms)
	PreparationTimeMs int64 `json:"preparation_time_ms,omitempty"`

	// The runtime of the job (ms)
	ElapsedTimeMs int64 `json:"elapsed_time_ms,omitempty"  bson:"elapsed_time_ms"`

	// The length of time taken to upload the job's outputs (ms)
	UploadTimeMs int64 `json:"upload_time_ms,omitempty"`

	// The total length of time from start to finish (ms)
	TotalTimeMs int64 `json:"total_time_ms,omitempty"`

	// Information about the job executor
	Executor *JobExecutorProfile `json:"executor,omitempty"`

	// Known version info for components involved in the job
	Versions map[string]string `json:"version,omitempty"`
}

type JobExecutorProfile struct {
	// A machine or instance name, possibly a FQDN
	Name string `json:"name,omitempty"`

	// The hostname or (more likely) IP address of the engine instance
	Host string `json:"host,omitempty"`

	// A free form string describing the instance type
	InstanceType string `json:"instance_type,omitempty"`

	// The number of CPU cores
	CpuCores int `json:"cpu_cores,omitempty"`

	// Whether or not a GPU is available
	Gpu bool `json:"gpu,omitempty"`

	// The amount of memory on the system, in bytes
	MemoryBytes int64 `json:"memory_bytes,omitempty"`

	// The size of the hard disk on the system, in bytes
	DiskBytes int64 `json:"disk_bytes,omitempty"`

	// The available swap space, in bytes
	SwapBytes int64 `json:"swap_bytes,omitempty"`
}

type JobLogStatement struct {
	// The file descriptor the log line came from.
	// Negative values are external logs from Flywheel components;
	// One is standard out;
	// Two is standard err;
	// Other values are invalid.
	FileDescriptor int8 `json:"fd"`

	// The message for this statement. Typically newline-delimited.
	Message string `json:"msg"`
}

type JobLog struct {
	Id   string             `json:"_id,omitempty"`
	Logs []*JobLogStatement `json:"logs,omitempty"`
}

// Get all jobs endpoint is not implemented as it returns a different format
// https://github.com/scitran/core/issues/704

func (c *Client) GetJob(id string) (*Job, *http.Response, error) {
	var aerr *Error
	var job *Job

	resp, err := c.New().Get("jobs/"+id).Receive(&job, &aerr)
	return job, resp, Coalesce(err, aerr)
}

func (c *Client) GetJobLogs(id string) (*JobLog, *http.Response, error) {
	var aerr *Error
	var logs *JobLog

	resp, err := c.New().Get("jobs/"+id+"/logs").Receive(&logs, &aerr)
	return logs, resp, Coalesce(err, aerr)
}

func (c *Client) AddJob(job *Job) (string, *http.Response, error) {
	var aerr *Error
	var response *IdResponse
	var result string

	resp, err := c.New().Post("jobs/add").BodyJSON(job).Receive(&response, &aerr)

	if response != nil {
		result = response.Id
	}

	return result, resp, Coalesce(err, aerr)
}

func (c *Client) AddJobLogs(id string, statements []*JobLogStatement) (*http.Response, error) {
	var aerr *Error

	resp, err := c.New().Post("jobs/"+id+"/logs").BodyJSON(statements).Receive(nil, &aerr)
	return resp, Coalesce(err, aerr)
}

func (c *Client) ModifyJob(id string, job *Job) (*http.Response, error) {
	var aerr *Error

	resp, err := c.New().Put("jobs/"+id).BodyJSON(job).Receive(nil, &aerr)
	return resp, Coalesce(err, aerr)
}

func (c *Client) JobsAsk(question *JobsQuestion) (*JobsAnswer, *http.Response, error) {
	var aerr *Error
	var result *JobsAnswer

	// Prevent server-side decoding issues
	if question.Whitelist == nil {
		question.Whitelist = &JobsMatch{}
	}
	if question.Blacklist == nil {
		question.Blacklist = &JobsMatch{}
	}

	resp, err := c.New().Post("jobs/ask").BodyJSON(question).Receive(&result, &aerr)
	rerr := Coalesce(err, aerr)

	return result, resp, rerr
}

// Start up to maxJobs pending jobs matching a set of properties.
func (c *Client) StartJobs(whitelist, blacklist *JobsMatch, capabilities []string, peek bool, maxJobs int) (JobRetrieval, []*Job, *http.Response, error) {
	ask := &JobsQuestion{
		Whitelist:    whitelist,
		Blacklist:    blacklist,
		Capabilities: capabilities,
		Return: &JobsAskType{
			Jobs:    maxJobs,
			Peek:    peek,
			Encoded: true,
		},
	}

	result, resp, err := c.JobsAsk(ask)

	if err == nil && result != nil && result.Jobs != nil {
		// Endpoint performed as expected. Translate to JobRetrieval for convenience.

		if len(result.Jobs) > 0 {
			return JobsAquired, result.Jobs, resp, nil
		} else {
			return NoPendingJobs, result.Jobs, resp, nil
		}
	} else {
		return JobFailure, nil, resp, err
	}
}

func (c *Client) GetJobStats(whitelist, blacklist *JobsMatch, capabilities []string, maxJobs int) (*JobStateCount, *http.Response, error) {
	ask := &JobsQuestion{
		Whitelist:    whitelist,
		Blacklist:    blacklist,
		Capabilities: capabilities,
		Return: &JobsAskType{
			States: true,
		},
	}

	result, resp, err := c.JobsAsk(ask)

	if err == nil && result != nil && result.States != nil {
		return result.States, resp, err
	} else {
		return nil, resp, err
	}
}

func (c *Client) HeartbeatJob(id string) (*http.Response, error) {
	var aerr *Error

	// Send empty modification
	empty := map[string]string{}

	resp, err := c.New().Put("jobs/"+id).BodyJSON(empty).Receive(nil, &aerr)
	return resp, Coalesce(err, aerr)
}

func (c *Client) ChangeJobState(id string, state JobState) (*http.Response, error) {
	var aerr *Error
	jobMod := &Job{
		State: state,
	}

	resp, err := c.New().Put("jobs/"+id).BodyJSON(jobMod).Receive(nil, &aerr)
	return resp, Coalesce(err, aerr)
}

// Create a job completion ticket.
// ref: https://github.com/flywheel-io/core/pull/1503#issue-244236118
func (c *Client) PrepareCompleteJob(id string) (string, *http.Response, error) {

	var aerr *Error
	var response *JobCompletionTicketResponse
	var result string

	resp, err := c.New().Post("jobs/"+id+"/prepare-complete").Receive(&response, &aerr)

	if response != nil {
		result = response.Ticket
	}

	return result, resp, Coalesce(err, aerr)
}

// Post job completion details
func (c *Client) CompleteJob(id, ticket string, completion *JobCompletion) (*http.Response, error) {
	var aerr *Error

	resp, err := c.New().Post("jobs/"+id+"/complete?job_ticket_id="+ticket).BodyJSON(completion).Receive(nil, &aerr)
	return resp, Coalesce(err, aerr)
}

// LEGACY

func (c *Client) StartNextPendingJobLegacy(peek bool, tags ...string) (JobRetrieval, *Job, *http.Response, error) {
	var aerr *Error
	var job *Job

	params := &struct {
		Tags []string `url:"tags,omitempty"`
		Peek bool     `url:"peek,omitempty"`
	}{
		Tags: tags,
		Peek: peek,
	}

	resp, err := c.New().Get("jobs/next").QueryStruct(params).Receive(&job, &aerr)
	rerr := Coalesce(err, aerr)

	if rerr == nil && job != nil {
		return JobsAquired, job, resp, nil
	} else if rerr != nil && resp != nil && resp.StatusCode == 400 {
		return NoPendingJobs, nil, resp, nil
	} else {
		return JobFailure, nil, resp, rerr
	}
}
