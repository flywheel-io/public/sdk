package api

import (
	"errors"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// Helper func
func (c *Client) modifyFileAttrs(url string, attributes *FileFields) (*http.Response, *ModifiedAndJobsResponse, error) {
	var aerr *Error
	var response *ModifiedAndJobsResponse

	resp, err := c.New().Put(url).BodyJSON(attributes).Receive(&response, &aerr)

	// Should not have to check this count
	// https://github.com/scitran/core/issues/680
	if err == nil && aerr == nil && response.ModifiedCount != 1 {
		return resp, nil, errors.New("Modifying file returned " + strconv.Itoa(response.ModifiedCount) + " instead of 1")
	}

	return resp, response, Coalesce(err, aerr)
}

func (c *Client) modifyFileClassification(url string, classification map[string]Classification) (*http.Response, *ModifiedAndJobsSpawned, error) {
	var aerr *Error
	var response *ModifiedAndJobsSpawned

	resp, err := c.New().Post(url).BodyJSON(classification).Receive(&response, &aerr)

	if err == nil && aerr == nil && response.ModifiedCount != 1 {
		return resp, nil, errors.New("Modifying file returned " + strconv.Itoa(response.ModifiedCount) + " instead of 1")
	}

	return resp, response, Coalesce(err, aerr)
}

// Helper func
func (c *Client) setInfo(url string, set map[string]interface{}, expectResponse bool) (*http.Response, error) {
	body := map[string]interface{}{
		"set": set,
	}
	return c.postWithOptionalModifiedResponse(url, body, expectResponse)
}

// Helper func
func (c *Client) replaceInfo(url string, replace map[string]interface{}, expectResponse bool) (*http.Response, error) {
	body := map[string]interface{}{
		"replace": replace,
	}
	return c.postWithOptionalModifiedResponse(url, body, expectResponse)
}

// Helper func
func (c *Client) deleteInfoFields(url string, keys []string, expectResponse bool) (*http.Response, error) {
	body := map[string]interface{}{
		"delete": keys,
	}
	return c.postWithOptionalModifiedResponse(url, body, expectResponse)
}

// Helper func
func (c *Client) deleteFile(url string) (*http.Response, error) {
	var aerr *Error
	var response *ModifiedResponse

	resp, err := c.New().Delete(url).Receive(&response, &aerr)

	// Should not have to check this count
	// https://github.com/scitran/core/issues/680
	if err == nil && aerr == nil && response.ModifiedCount != 1 {
		return resp, errors.New("Deleting " + url + " returned " + strconv.Itoa(response.ModifiedCount) + " instead of 1")
	}

	return resp, Coalesce(err, aerr)
}

// Helper func
func (c *Client) addContainerAnalysis(url string, analysis *AnalysisInput) (string, *http.Response, error) {
	var aerr *Error
	var response *IdResponse
	var result string

	// Validate that input/job are not both set
	if analysis.Job != nil && analysis.Inputs != nil {
		return "", nil, errors.New("Cannot specify both inputs and jobs when creating analysis.")
	}

	resp, err := c.New().Post(url).BodyJSON(analysis).Receive(&response, &aerr)

	if response != nil {
		result = response.Id
	}

	return result, resp, Coalesce(err, aerr)
}

// Helper func
func (c *Client) postWithOptionalModifiedResponse(url string, body interface{}, expectResponse bool) (*http.Response, error) {
	var aerr *Error
	var resp *http.Response
	var err error

	if expectResponse {
		var response *ModifiedResponse
		resp, err = c.New().Post(url).BodyJSON(body).Receive(&response, &aerr)

		if err == nil && aerr == nil && response.ModifiedCount != 1 {
			// Should not have to check this count
			// https://github.com/scitran/core/issues/680
			return resp, errors.New("Modifying file returned " + strconv.Itoa(response.ModifiedCount) + " instead of 1")
		}
	} else {
		resp, err = c.New().Post(url).BodyJSON(body).Receive(nil, &aerr)
	}

	return resp, Coalesce(err, aerr)
}

// ParseApiKey accepts an API key and returns the hostname, port, key, and any parsing error.
func ParseApiKey(apiKey string) (string, int, string, error) {
	var err error
	host := ""
	port := 443
	key := ""

	splits := strings.Split(apiKey, ":")

	if len(splits) < 2 {
		return host, port, key, errors.New("Invalid API key")
	}

	if len(splits) == 2 {
		host = splits[0]
		key = splits[1]
	} else {
		host = splits[0]
		port, err = strconv.Atoi(splits[1])
		key = splits[len(splits)-1]
	}

	return host, port, key, err
}

// Config represents some of the server's configuration.
type Config struct {
	// Auth holds information about how users can authenticate.
	//
	// NOTE: this can go one layer deeper after multi-auth.
	// https://github.com/scitran/core/pull/652
	Auth map[string]interface{} `json:"auth"`

	// Site holds multi-site registration information.
	// This feature is depreciated.
	Site map[string]interface{} `json:"site"`

	// Set of features advertised by core
	Features map[string]bool `json:"features",omitempty"`

	Created  time.Time `json:"created"`
	Modified time.Time `json:"modified"`
}

// The API sometimes returns arbitrary-typed features.
// In the short term, drop non-boolean keys.
// In the long  term, a better approach may be needed.
type configActual struct {
	Auth map[string]interface{} `json:"auth"`
	Site map[string]interface{} `json:"site"`
	// The features key is hard-assumed to at least be a map.
	Features map[string]interface{} `json:"features",omitempty"`
	Created  time.Time              `json:"created"`
	Modified time.Time              `json:"modified"`
}

func (c *Client) GetConfig() (*Config, *http.Response, error) {
	var aerr *Error
	var configRaw *configActual
	resp, err := c.New().Get("config").Receive(&configRaw, &aerr)
	cErr := Coalesce(err, aerr)

	// Copy values over
	config := &Config{
		Auth:     configRaw.Auth,
		Site:     configRaw.Site,
		Features: map[string]bool{}, // empty until mapping over
		Created:  configRaw.Created,
		Modified: configRaw.Modified,
	}

	// See above notes on configActual type for reasoning.
	if cErr == nil && configRaw != nil {
		for key, value := range configRaw.Features {
			switch v := value.(type) {
			case bool:
				config.Features[key] = v
			}
		}
	}

	return config, resp, cErr
}

// Version identifies the upgrade level of system components.
type Version struct {
	// Database represents the database schema level.
	Database int `json:"database"`
}

func (c *Client) GetVersion() (*Version, *http.Response, error) {
	var aerr *Error
	var version *Version
	resp, err := c.New().Get("version").Receive(&version, &aerr)
	return version, resp, Coalesce(err, aerr)
}

type AuthStatus struct {
	Admin  *bool    `json:"user_is_admin,omitempty"`
	Device *bool    `json:"is_device,omitempty"`
	Origin *Origin  `json:"origin,omitempty"`
	Roles  []string `json:"roles,omitempty"`
}

// GetAuthStatus validates the current credentials.
func (c *Client) GetAuthStatus() (*AuthStatus, *http.Response, error) {
	var aerr *Error
	var response *AuthStatus

	resp, err := c.New().Get("auth/status").Receive(&response, &aerr)
	return response, resp, Coalesce(err, aerr)
}
