package api

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"syscall"
)

// Returns a JobExecutorProfile with some of its fields filled in.
func CreateLocalJobExecutorProfile(path string) *JobExecutorProfile {

	profile := &JobExecutorProfile{
		CpuCores: runtime.NumCPU(),
	}

	profile.Name, _ = os.Hostname()

	// Fill in memory info if possible
	info, err := getMemInfo()
	if err == nil {

		// Total memory
		raw, ok := info["MemTotal_bytes"]
		if ok {
			profile.MemoryBytes = int64(raw)
		}

		// Total memory
		raw, ok = info["SwapTotal_bytes"]
		if ok {
			profile.SwapBytes = int64(raw)
		}
	}

	// Fill in disk info if possible
	if path != "" {
		var stat syscall.Statfs_t
		err = syscall.Statfs(path, &stat)
		if err == nil {
			// Number of blocks * size of a block
			profile.DiskBytes = int64(stat.Blocks) * stat.Bsize
		}
	}

	return profile
}

// Below is taken from:
// https://github.com/prometheus/procfs
// https://github.com/prometheus/node_exporter
//
// Both under Apache 2, which our license is compatible with.
// Unexported fields and prometheus Collector design make this challenging to use directly.

const DefaultMountPoint = "/proc"

func procFilePath(name string) string {
	return path.Join(DefaultMountPoint, name)
}

func getMemInfo() (map[string]float64, error) {
	file, err := os.Open(procFilePath("meminfo"))
	if err != nil {
		return nil, err
	}
	defer file.Close()

	return parseMemInfo(file)
}

func parseMemInfo(r io.Reader) (map[string]float64, error) {
	var (
		memInfo = map[string]float64{}
		scanner = bufio.NewScanner(r)
		re      = regexp.MustCompile(`\((.*)\)`)
	)

	for scanner.Scan() {
		line := scanner.Text()
		parts := strings.Fields(line)
		fv, err := strconv.ParseFloat(parts[1], 64)
		if err != nil {
			return nil, fmt.Errorf("invalid value in meminfo: %s", err)
		}
		key := parts[0][:len(parts[0])-1] // remove trailing : from key
		// Active(anon) -> Active_anon
		key = re.ReplaceAllString(key, "_${1}")
		switch len(parts) {
		case 2: // no unit
		case 3: // has unit, we presume kB
			fv *= 1024
			key = key + "_bytes"
		default:
			return nil, fmt.Errorf("invalid line in meminfo: %s", line)
		}
		memInfo[key] = fv
	}

	return memInfo, scanner.Err()
}
