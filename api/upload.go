package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/juju/ratelimit"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"
)

// Use a 16 MB bucket size for rate-limiting
const UploadBucketSize = 16777216 // 16 MB
// Max number of attempts when signed url upload fails
const SignedUrlUploadMaxAttempts = 3

// Duration in nanoseconds to sleep between signed url upload attempts
const RetryDelay = time.Duration(5 * time.Second) // 5 seconds

// UploadSource represents one file to upload.
//
// It is only valid to set one of (Reader, Path).
// If Path is set, it will be read off disk using os.Open.
//
// If Name is not set, then filepath.Base(Path) will be used.
type UploadSource struct {
	Name string

	Reader io.ReadCloser
	Path   string
}

type UploadBody struct {
	Metadata  map[string]interface{} `json:"metadata"`
	Filenames []string               `json:"filenames,omitempty"`
}

type TicketResponse struct {
	Ticket  string            `json:"ticket,omitempty"`
	Headers map[string]string `json:"headers,omitempty"`
	Urls    map[string]string `json:"urls,omitempty"`
}

// Bundle an http response and error together for returning over a channel
type UploadResponse struct {
	Response *http.Response
	Error    error
}

func CreateUploadSourceFromFilenames(filenames ...string) []*UploadSource {
	var sources = make([]*UploadSource, len(filenames))

	for x, filename := range filenames {
		sources[x] = &UploadSource{Path: filename, Name: filepath.Base(filename)}
	}

	return sources
}

func ensureFileNames(files []*UploadSource) error {
	for _, file := range files {
		// Name the file, if no name was given.
		if file.Name == "" {
			if file.Path == "" {
				return errors.New("Neither file name nor path was set in upload source")
			}
			file.Name = filepath.Base(file.Path)
		}
	}
	return nil
}

// Write a set of UploadSources to a multipart writer, reporting progress to a ProgressReader.
// rate is an optional upload transfer limit, in MB/sec. A rate of 0 will result in an unlimited upload
func writeUploadSources(writer *multipart.Writer, reader *ProgressReader, metadata []byte, rate float64, files []*UploadSource) error {
	defer func() {
		writer.Close()
		reader.Close()
	}()

	// Add metadata, if any
	if len(metadata) > 0 {
		mWriter, err := writer.CreateFormField("metadata")
		if err != nil {
			return fmt.Errorf("CreateFormField metadata error -- %s", err)
		}
		_, err = mWriter.Write(metadata)
		if err != nil {
			return fmt.Errorf("Write metadata error -- %s", err)
		}
	}

	err := ensureFileNames(files)
	if err != nil {
		return err
	}

	for i, file := range files {
		// Open a file descriptor if this UploadSource was not already an open reader
		if file.Reader == nil {
			fileReader, err := os.Open(file.Path)
			file.Reader = fileReader

			if err != nil {
				return fmt.Errorf("Error opening %s -- %s", file.Name, err)
			}
		}
		defer file.Reader.Close()

		// Report progress of the uploads, not of the encoded stream.
		// Upload progress of metadata and preamble will not be reported.
		reader.SetReader(file.Reader)

		// Create a form name for this file.
		// If there's only one file, don't add an index.
		// It might be valid to upload without this check. Worth testing.
		formTitle := "file"
		if len(files) > 1 {
			formTitle = strings.Join([]string{"file", strconv.Itoa(i + 1)}, "")
		}

		// Create a form entry for this file
		fileWriter, err := writer.CreateFormFile(formTitle, file.Name)
		if err != nil {
			return fmt.Errorf("CreateFormFile %s error -- %s", file.Name, err)
		}

		// Copy the file
		if rate > 0 {
			bucket := ratelimit.NewBucketWithRate(rate, UploadBucketSize)
			_, err = io.Copy(fileWriter, ratelimit.Reader(reader, bucket))
		} else {
			_, err = io.Copy(fileWriter, reader)
		}

		if err != nil && err != io.EOF {
			return fmt.Errorf("io.Copy %s error -- %s", file.Name, err)
		}

		file.Reader.Close()
	}

	return nil
}

// Fire an upload with a given url, reader, and content type.
func (c *Client) sendUploadRequest(url string, reader io.ReadCloser, contentType string) (*http.Response, error) {
	req, err := c.New().Post(url).
		Body(reader).
		Set("Content-Type", contentType).
		Request()

	if err != nil {
		return nil, err
	}

	resp, err := c.Doer.Do(req)
	if err != nil {
		return resp, err
	}

	if resp.StatusCode != 200 {
		// Needs robust handling for body & raw nils
		raw, _ := ioutil.ReadAll(resp.Body)

		return resp, errors.New(string(raw))
	}

	return resp, err
}

func (c *Client) SignedUrlUpload(ticket TicketResponse, files ...*UploadSource) (chan int64, chan error) {
	var resp *http.Response
	var err error

	// The raw progress channel
	progress := make(chan int64, len(files))

	// Report result back to caller, If there's an error we can't handle, we should just continue and send it to the channel
	resultChan := make(chan error, len(files))

	// Loops through files to be uploaded and uploads them to the corresponding presigned urls
	go func() {
		for currentProgress, file := range files {
			var fileLength int64 = 0

			if file.Reader == nil {
				var fileInfo os.FileInfo
				fileInfo, err = os.Stat(file.Path)
				if err == nil {
					fileLength = fileInfo.Size()
					file.Reader, err = os.Open(file.Path)
				}
				if err != nil {
					err = fmt.Errorf("Open %s failed -- %s", file.Name, err)
					resultChan <- err
					continue
				}
			}

			request, err := http.NewRequest("PUT", ticket.Urls[file.Name], file.Reader)
			request.ContentLength = fileLength
			if ticket.Headers != nil {
				for header, value := range ticket.Headers {
					request.Header.Add(header, value)
				}
			}
			if err != nil {
				err = fmt.Errorf("Signed URL request failed for %s -- %s", file.Name, err)
				resultChan <- err
				continue
			}

			resp, err = c.UploadWithRetry(request, RetryDelay)
			if err != nil {
				err = fmt.Errorf("UploadWithRetry failed for %s -- %s", file.Name, err)
				resultChan <- err
				continue
			}

			if resp.StatusCode != 200 && resp.StatusCode != 201 {
				err = fmt.Errorf("Request returned a %d for %s after %d attempts",
					resp.StatusCode, file.Name, SignedUrlUploadMaxAttempts)
			}
			resultChan <- err
			progress <- int64(currentProgress)
		}
		close(progress)
		close(resultChan)
	}()
	return progress, resultChan
}

// Retries the signed url upload if encountering 500 http responses
func (c *Client) UploadWithRetry(request *http.Request, retryDelay time.Duration) (*http.Response, error) {
	var resp *http.Response
	var err error
	var attempts int

	fileReadSeeker, _ := request.Body.(io.ReadSeeker)
	closer, _ := request.Body.(io.ReadCloser)

	// Close at the end of the function, but not before
	if closer != nil {
		defer closer.Close()
		request.Body = ioutil.NopCloser(request.Body)
	}

	attempts = 1
	for true {
		resp, err = c.Doer.Do(request)

		// Retry if we get 5xx or a transport error (err != nil)
		// Break if we get 200 or 4xx
		if err == nil {
			if resp.StatusCode != 200 && resp.StatusCode != 201 {
				var body []byte = nil
				var status string = "<none>"
				if resp != nil {
					status = resp.Status
					if resp.Body != nil {
						body, _ = ioutil.ReadAll(resp.Body)
					}
				}
				err = fmt.Errorf("status=%v, attempts=%d, body=%s", status, attempts, string(body))
			}
			if resp.StatusCode < 500 || attempts >= SignedUrlUploadMaxAttempts {
				break
			}
		}

		// Break if we cannot retry due to invalid seeker
		if fileReadSeeker == nil {
			break
		}
		_, seekErr := fileReadSeeker.Seek(0, io.SeekStart)
		if seekErr != nil {
			break
		}

		time.Sleep(retryDelay)
		attempts += 1
	}

	return resp, err
}

// Upload with a limited transfer speed
// rate is an optional upload transfer limit, in MB/sec. A rate of 0 will result in an unlimited upload
func (c *Client) UploadWithRate(url string, metadata []byte, progress chan<- int64, rate float64, files []*UploadSource) chan error {

	// Form data is written from one goroutine to another
	reader, writer := io.Pipe()
	multipartWriter := multipart.NewWriter(writer)
	contentType := multipartWriter.FormDataContentType()

	// Wrap the pipe in a ProgressReader.
	progressReader := NewProgressReader(nil, progress)

	// Shared memory for results, protected by a waitgroup. Simpler (but more dangerous) than channels.
	var writeError error
	var uploadError error
	var response *http.Response
	var wg sync.WaitGroup
	wg.Add(2)

	// Report result back to caller
	resultChan := make(chan error, 1)

	// Stream multipart encoding
	go func() {
		writeError = writeUploadSources(multipartWriter, progressReader, metadata, rate, files)
		writer.Close()
		wg.Done()
	}()

	// Send encoded body to server, await completion, report
	go func() {
		response, uploadError = c.sendUploadRequest(url, reader, contentType)
		wg.Done()
	}()

	// Wait for both to complete, and report back
	go func() {
		wg.Wait()
		if response != nil && response.Body != nil {
			response.Body.Close()
		}

		// Encoding & local-IO errors take precedence over network errors.
		// Could combine the two if both are set. Eh.
		if writeError != nil {
			resultChan <- writeError
		} else {
			resultChan <- uploadError
		}
		close(resultChan)
	}()

	return resultChan
}

// Upload will send a set of UploadSources to url, reporting uploaded bytes to progress if set.
// Upload will not block sending to progress.
//
// Depending on the URL, metadata may be required, or only one file may be allowed at a time.
// It is generally a good idea to use a purpose-specific upload method.
func (c *Client) Upload(url string, metadata []byte, progress chan<- int64, files []*UploadSource) chan error {
	return c.UploadWithRate(url, metadata, progress, 0, files)
}

// UploadSimple is a convenience wrapper around Upload.
// Passing 0 for rate will result in an unlimited transfer
// It creates the progress channel and UploadSource array for you.
func (c *Client) UploadSimpleWithRate(url string, metadata []byte, rate float64, files ...*UploadSource) (chan int64, chan error) {

	progress := make(chan int64, 10)

	return progress, c.UploadWithRate(url, metadata, progress, rate, files)
}

// UploadSimpleWithRate is a convenience wrapper around UploadWithRate.
func (c *Client) UploadSimple(url string, metadata []byte, files ...*UploadSource) (chan int64, chan error) {
	return c.UploadSimpleWithRate(url, metadata, 0, files...)
}

func (c *Client) UploadSimpleSignedUrl(url string, metadata []byte, files ...*UploadSource) (chan int64, chan error) {
	var progress chan int64
	var errChan chan error
	var intermediateErrChan chan error
	var completeTicket bool
	var err error
	var ticketURL string

	// The lovely core uses different ticketing conventions depending on the endpoint
	// The convention appears to be that if the path starts with "upload" then we should use
	// ticket, otherwise we should use "upload_ticket" (e.g. engine uploads)
	if strings.HasPrefix(url, "upload") {
		ticketURL = url + "&ticket="
	} else {
		ticketURL = url + "&upload_ticket="
	}

	ticket, _, _ := c.CreateUploadTicket(ticketURL, metadata, files...)
	if ticket != nil {
		errChan = make(chan error, len(files))
		progress, intermediateErrChan = c.SignedUrlUpload(*ticket, files...)
		go func() {
			completeTicket = true
			for err = range intermediateErrChan {
				if err != nil {
					completeTicket = false
				}
				errChan <- err
			}
			if completeTicket {
				_, _, err = c.CompleteUploadTicket(ticketURL, ticket.Ticket)
				if err != nil {
					errChan <- err
				}
			}
			close(errChan)
		}()
	} else {
		progress, errChan = c.UploadSimple(url, metadata, files...)
	}
	return progress, errChan
}

// CreateUploadTicket creates an upload ticket
func (c *Client) CreateUploadTicket(url string, metadata []byte, files ...*UploadSource) (*TicketResponse, *http.Response, error) {
	var aerr *Error
	var response *TicketResponse
	var metadataMap map[string]interface{}

	json.Unmarshal(metadata, &metadataMap)

	err := ensureFileNames(files)
	if err != nil {
		return nil, nil, err
	}

	filenames := make([]string, len(files))
	for i, file := range files {
		filenames[i] = file.Name
	}
	uploadTicketBody := UploadBody{Metadata: metadataMap, Filenames: filenames}
	resp, err := c.New().Post(url).BodyJSON(uploadTicketBody).Receive(&response, &aerr)

	return response, resp, Coalesce(err, aerr)
}

func (c *Client) CompleteUploadTicket(url string, ticketId string) ([]*File, *http.Response, error) {
	var aerr *Error
	var files []*File

	url += ticketId
	resp, err := c.New().Post(url).Receive(&files, &aerr)
	return files, resp, Coalesce(err, aerr)
}
