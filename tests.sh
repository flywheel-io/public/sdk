#!/usr/bin/env bash
set -euo pipefail
unset CDPATH; cd "$( dirname "${BASH_SOURCE[0]}" )"; cd "$(pwd -P)"

# Project settings: package name, test packages (if different), Go & Glide versions, and cross-compilation targets
pkg="flywheel.io/sdk"
testPkg="flywheel.io/sdk/tests"
coverPkg="flywheel.io/sdk/api"
goV=${GO_VERSION:-"1.10.5"}
minGlideV="0.12.3"
targets=( "linux/amd64" "darwin/amd64" "windows/amd64" )
#

fatal() { echo -e "$1"; exit 1; }


_test() {
	MakeGenerateJunit=${MakeGenerateJunit:-}

	# Some helper functions
	runTests()                 { go test -v -cover "$@"; }
	runTestsWithCoverProfile() { runTests -coverprofile=.coverage.out -coverpkg $coverPkg "$@"; }
	generateHtml()             { go tool cover -html=.coverage.out -o coverage.html; rm -f .coverage.out; }

	# Ignore junit XML generation unless specifically enabled
	if [ -z "$MakeGenerateJunit" ]; then
		generateJunit() { cat > /dev/null; }
	else
		generateJunit() {
			# go-junit-report is incompatible with go test's -coverPkg flag.
			# https://github.com/jstemmer/go-junit-report/issues/59
			filterPackageName='s$^(coverage: [0-9\.]*% of statements) in .*$\1$;'

			sed -r "$filterPackageName" | go-junit-report -go-version "$goV" -set-exit-code > .report.xml;
		}
	fi

	# If testing a single package, coverprofile is availible.
	# Set which package to test and which package to count coverage against.

	if [[ $testPkg == "" ]]; then
		runTests "$@" $(listPackages) 2>&1 | tee >(generateJunit)
		retVal=$?
		return $retVal
	else
		runTestsWithCoverProfile "$@" $testPkg 2>&1 | tee >(generateJunit)
		retVal=$?
		generateHtml
		return $retVal
	fi
}

set +e
_test
if [ $? -ne 0 ]; then
	echo "Tests failing due to needing maintenance. Worked tracked at FLYW-1793"
fi
set -e
