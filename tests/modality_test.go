package tests

import (
	. "github.com/smartystreets/assertions"

	"flywheel.io/sdk/api"
)

func (t *F) TestModalities() {
	modalityId := RandString()
	modality := &api.Modality{
		Id:             modalityId,
		Classification: api.Classification{"Numbers": []string{"One", "Two"}, "Letters": []string{"A", "B"}},
	}

	// Add
	_, err := t.AddModality(modality)
	t.So(err, ShouldBeNil)

	// Get
	rModality, _, err := t.GetModality(modalityId)
	t.So(err, ShouldBeNil)
	t.So(rModality.Id, ShouldEqual, modalityId)
	t.So(rModality.Classification, ShouldResemble, modality.Classification)

	// Get all
	_, _, err = t.GetAllModalities()
	t.So(err, ShouldBeNil)

	// Replace
	modality = &api.Modality{
		Id:             modalityId,
		Classification: api.Classification{"Numbers": []string{"Three", "Four"}, "Letters": []string{"A", "B"}},
	}
	_, err = t.ReplaceModality(modality)
	t.So(err, ShouldBeNil)

	rModality, _, err = t.GetModality(modalityId)
	t.So(err, ShouldBeNil)
	t.So(rModality.Id, ShouldEqual, modalityId)
	t.So(rModality.Classification, ShouldResemble, modality.Classification)

	// Delete
	_, err = t.DeleteModality(modalityId)
	t.So(err, ShouldBeNil)

	rModality, _, err = t.GetModality(modalityId)
	t.So(err, ShouldNotBeNil)
	t.So(rModality, ShouldBeNil)
}

func (t *F) TestFileClassifications() {
	// Add modality
	modalityId := RandString()
	modality := &api.Modality{
		Id:             modalityId,
		Classification: api.Classification{"Numbers": []string{"One", "Two"}, "Letters": []string{"A", "B"}},
	}
	_, err := t.AddModality(modality)
	t.So(err, ShouldBeNil)

	// Add file
	_, _, _, acquisitionId := t.createTestAcquisition()

	poem := "Turning and turning in the widening gyre"
	t.uploadText(t.UploadToAcquisition, acquisitionId, "yeats.txt", poem)
	_, _, err = t.ModifyAcquisitionFile(acquisitionId, "yeats.txt", &api.FileFields{
		Modality: modalityId,
		Type:     "type",
	})
	t.So(err, ShouldBeNil)

	// Unkown modality means files can only have items under Custom
	_, _, err = t.AddAcquisitionFileClassification(acquisitionId, "yeats.txt",
		api.Classification{"Contrast": []string{"Text", "Random"}})
	t.So(err, ShouldNotBeNil)

	// Replace the entire classification
	_, _, err = t.ReplaceAcquisitionFileClassification(acquisitionId, "yeats.txt",
		api.Classification{"Custom": []string{"newMeasurement"}})
	t.So(err, ShouldBeNil)
	rAcquisition, _, err := t.GetAcquisition(acquisitionId)
	t.So(err, ShouldBeNil)
	t.So(rAcquisition.Files[0].Classification["Custom"], ShouldHaveLength, 1)
	t.So(rAcquisition.Files[0].Classification["Custom"][0], ShouldEqual, "newMeasurement")

	// Attempt to remove nonexistent classification "measurement", it will still return true
	_, _, err = t.RemoveAcquisitionFileClassification(acquisitionId, "yeats.txt",
		api.Classification{"Custom": []string{"measurement"}})
	t.So(err, ShouldBeNil)

	// Remove classification
	_, _, err = t.RemoveAcquisitionFileClassification(acquisitionId, "yeats.txt",
		api.Classification{"Custom": []string{"newMeasurement"}})
	t.So(err, ShouldBeNil)

	rAcquisition, _, err = t.GetAcquisition(acquisitionId)
	t.So(err, ShouldBeNil)
	t.So(rAcquisition.Files[0].Classification["Custom"], ShouldHaveLength, 0)
}
