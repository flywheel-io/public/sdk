BASE                = $(GOPATH)/src/$(PACKAGE)
SRC                 = $(shell find . -type f -name '*.go' -not -path "./vendor/*" -not -path "./builds/*")
GLIDE               = glide
GO                  = go
GOFMT               = $(GO)fmt
GOPATH              = $(CURDIR)/.gopath
PACKAGE             = flywheel.io/sdk

.PHONY: all
all: fmt vendor | $(BASE)
	cd $(BASE) && $(GO) build -o "bin/$(PACKAGE)" $(PACKAGE)

$(BASE):
	@mkdir -p $(dir $@)
	@ln -sf $(CURDIR) $@

.PHONY: fmt
fmt: ; $(info $(M) running gofmt…) @ ## Run gofmt on all source files
	@$(GOFMT) -l -w $(SRC)

.PHONY: glide.lock
glide.lock: glide.yaml | $(BASE)
	cd $(BASE) && $(GLIDE) update
	@touch $@

vendor: glide.lock | $(BASE)
	$(info retrieving dependencies...)
	cd $(BASE) && $(GLIDE) --quiet install
	@ln -nsf . vendor/src
	@touch $@

.PHONY: clean
clean: ; $(info $(M) cleaning...)	@ ## Cleanup everything
	rm -rf bin/ vendor/
	rm -rf $(GOPATH)

test: vendor | $(BASE)
	./tests.sh

.PHONY: fmtCheck
fmtCheck:
	@if [ $(shell $(GOFMT) -l $(SRC)) ]; then echo "Format check failed"; exit 1; fi
