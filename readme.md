# Flywheel SDK

An SDK for interaction with a remote Flywheel instance in Golang!

[![GoDoc](https://godoc.org/github.com/flywheel-io/sdk/api?status.svg)](https://godoc.org/github.com/flywheel-io/sdk/api)
[![Report Card](https://goreportcard.com/badge/github.com/flywheel-io/sdk)](https://goreportcard.com/report/github.com/flywheel-io/sdk)
[![Build status](https://circleci.com/gh/flywheel-io/sdk/tree/master.svg?style=shield)](https://circleci.com/gh/flywheel-io/sdk)

## Building

```bash
git clone https://github.com/flywheel-io/sdk workspace/src/flywheel.io/sdk
ln -s workspace/src/flywheel.io/sdk sdk

./sdk/make.sh
```

## Testing

The simplest way to run the test suite is to install the [CircleCI runner](https://circleci.com/docs/2.0/local-jobs/#installation) and use it from the SDK folder:

```
circleci build
```

If you want to test manually, you can configure the test suite with these environment variables:

* `SdkTestKey`: Set this to an API key. Defaults to `localhost:8443:change-me`.
* `SdkTestMongo`: Set this to a mongo connection string. If not set, database tests are skipped.
* `SdkTestDebug`: Setting this to any value will cause each test to print an HTTP/1.1 representation of each request. Best used to debug a single failing test.

To run the integration test suite against a running API:

```bash
export SdkTestKey="localhost:8443:some-api-key"

./sdk/make.sh test
```

Or, to run a single test:

```bash
./sdk/make.sh test -run TestSuite/TestGetConfig
```
